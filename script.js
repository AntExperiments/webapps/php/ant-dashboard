var instance = M.Tabs.init(document.getElementsByClassName('tabs')[0]);

let isEnabledOctoprint = false;
let shownStatus = "none";

setInterval(tick , 750);
setInterval(updateStatus, 2000);
setInterval(updateReddit, 600000); // 10 minutes

updateStatus()
tick()
updateReddit()

function tick() {
    // update clock
    var date = new Date;
    document.getElementById('clockTime').innerText = date.getHours() + ":" + (date.getMinutes()<10?'0':'') + date.getMinutes();
    document.getElementById('clockDate').innerText = date.toISOString().substring(0, 10);

    // enable or disable octoprint
    var request = new XMLHttpRequest();
    request.open('GET', 'http://'+ octoprintIP +'/api/job', true);
    request.onload = function() {
        var response = JSON.parse(this.responseText);
        if (response.state == "Operational")
            isEnabledOctoprint = false
        else
            isEnabledOctoprint = true
    }
    request.setRequestHeader('X-Api-Key', octoprintApiKey);
    request.send();

    // update status
    if (shownStatus == "octoprint") {
        var request = new XMLHttpRequest();
        request.open('GET', 'http://'+ octoprintIP +'/api/job', true);
        request.onload = function() {
            var response = JSON.parse(this.responseText);
            document.getElementById('statusOctoprintFile').innerText = response.job.file.display;
            if (response.state == "Printing") {
                var date = new Date(0);
                date.setSeconds(response.progress.printTimeLeft);
                document.getElementById('statusOctoprintState').innerText = response.state + " " + date.toISOString().substr(11, 8) + " remaining";
            } else {
                document.getElementById('statusOctoprintState').innerText = response.state;
            }
            document.getElementById('statusOctoprintCompletition').style.width = response.progress.completion + "%";
        }
        request.setRequestHeader('X-Api-Key', octoprintApiKey);
        request.send();
    }


    // update AxxivServer - CPU
    var request = new XMLHttpRequest();
    request.open('GET', 'http://192.168.1.16:5002/cpu', true);
    request.onload = function() {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("axxivServerCPU").innerText = this.responseText.replace("\n", "") + "%";
        } else {
            document.getElementById("axxivServerCPU").innerText = "Offline";
        }
    }
    request.setRequestHeader('X-Api-Key', octoprintApiKey);
    request.send();

    // update AxxivServer - RAM
    var request = new XMLHttpRequest();
    request.open('GET', 'http://192.168.1.16:5002/ram', true);
    request.onload = function() {
        if (this.readyState == 4 && this.status == 200) {
            var response = JSON.parse(this.responseText);
            document.getElementById("axxivServerMemory").innerText = formatBytes(response.used, 1) + " / " + formatBytes(response.total, 1);
        } else {
            document.getElementById("axxivServerMemory").innerText = "Offline";
        }
    }
    request.setRequestHeader('X-Api-Key', octoprintApiKey);
    request.send();

    // update AxxivServer - Storage
    var request = new XMLHttpRequest();
    request.open('GET', 'http://192.168.1.16:5002/storage/root', true);
    request.onload = function() {
        if (this.readyState == 4 && this.status == 200) {
            var response = JSON.parse(this.responseText);
            document.getElementById("axxivServerStorage").innerText = formatBytes(response.used, 1) + " / " + formatBytes(response.total, 1);
        } else {
            document.getElementById("axxivServerStorage").innerText = "Offline";
        }
    }
    request.setRequestHeader('X-Api-Key', octoprintApiKey);
    request.send();



    // update Workstation - CPU
    var request = new XMLHttpRequest();
    request.open('GET', 'http://192.168.1.11:5002/cpu', true);
    request.onload = function() {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("workstationCPU").innerText = this.responseText.replace("\n", "") + "%";
        } else {
            document.getElementById("workstationCPU").innerText = "Offline";
        }
    }
    request.setRequestHeader('X-Api-Key', octoprintApiKey);
    request.send();

    // update Workstation - RAM
    var request = new XMLHttpRequest();
    request.open('GET', 'http://192.168.1.11:5002/ram', true);
    request.onload = function() {
        if (this.readyState == 4 && this.status == 200) {
            var response = JSON.parse(this.responseText);
            document.getElementById("workstationMemory").innerText = formatBytes(response.used, 1) + " / " + formatBytes(response.total, 1);
        } else {
            document.getElementById("workstationMemory").innerText = "Offline";
        }
    }
    request.setRequestHeader('X-Api-Key', octoprintApiKey);
    request.send();

    // update Workstation - Storage
    var request = new XMLHttpRequest();
    request.open('GET', 'http://192.168.1.11:5002/storage/root', true);
    request.onload = function() {
        if (this.readyState == 4 && this.status == 200) {
            var response = JSON.parse(this.responseText);
            document.getElementById("workstationStorage").innerText = formatBytes(response.used, 1) + " / " + formatBytes(response.total, 1);
        } else {
            document.getElementById("workstationStorage").innerText = "Offline";
        }
    }
    request.setRequestHeader('X-Api-Key', octoprintApiKey);
    request.send();
}

function updateStatus() {
    // don't show any statuses by default
    let show = false;

    if (isEnabledOctoprint == true) {
        show = true;
        shownStatus = "octoprint";
        document.getElementById('statusOctoprint').visibility = 'visible';
    }

    // move clock and hide status if nothing is beind displayed
    if (show == true) {
        document.getElementById('status').style.visibility = 'visible';
        document.getElementById('statusSpacerDiv').style.height = '0px';
    } else {
        document.getElementById('status').style.visibility = 'hidden';
        document.getElementById('statusSpacerDiv').style.height = '30px';
        shownStatus = "none";
    }
}

function updateReddit() {
    // create Snoowrap instance (interface between AntDashboard and Reddit (not made by me))
    var snoowrap = window.snoowrap;

    // authentificate
    const r = new snoowrap({
      userAgent: 'put your user-agent string here',
      clientId: redditClientId,
      clientSecret: redditClientSecret,
      refreshToken: redditRefreshToken
    });
    
    // getr user Data of cutie and display
    r.getUser('cAtloVeR9998').getSubmissions().then(listing => {
        titleArray = listing.map(submission => submission.title);
        imageArray = listing.map(submission => submission.url);
        output = "";
    
        for (let i = 0; i < 7; i++) {
            output += `
                <div class="card horizontal">
                    <div class="card-image">
                        <img src="${imageArray[i]}" style="width: 180px;height: 150px;object-fit: cover;" onerror="this.src='src/img/link.png';">
                        </div>
                        <div class="card-stacked">
                            <div class="card-content">
                                <h5>u/cAtloVeR9998</h5>
                                <p>${titleArray[i]}</p>
                            </div>
                        </div>
                    </div>
                </div>`;
            titleArray[i];
        }
    
        document.getElementById('redditContent').innerHTML = output;
    });
}












// https://stackoverflow.com/questions/15900485/correct-way-to-convert-size-in-bytes-to-kb-mb-gb-in-javascript
function formatBytes(bytes, decimals = 2) {
    if (bytes === 0) return '0 Bytes';

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}